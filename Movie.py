import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine


Base = declarative_base()


class Movie(Base):
     __tablename__ = 'movies'

     id = Column(Integer, primary_key=True)
     title = Column(String(255), nullable=False)
     year = Column(Integer)

     def __init__(self, title, year):
         self.title = title
         self.year = year

     def __repr__(self):
         return "Movie({}, {})".format(self.title, self.year)



engine = create_engine('mysql://markbaulo:!markbaulo@192.168.11.248/markbaulo')
Base = declarative_base()
