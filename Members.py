import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('mysql://markbaulo:!markbaulo@192.168.11.248/markbaulo')
Base = declarative_base()


class Members(Base):
     __tablename__ = 'Members'

     id = Column(Integer, primary_key=True)
     fname = Column(String(50))
     lname = Column(String(50))
     description = Column(String(200))

     def __init__(self, fname, lname, description):
         self.fname = fname
         self.description = description
         self.lname = lname

     def __repr__(self):
         return "Members({} {}, {})".format(self.fname, self.lname, self.description)

# Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
# # session.add_all([Members("Kevin","Ma","Nihao Ma"),Members("Neil","Mula","Bossing"),
# #                 Members("Noreen","Ras","Madam"),Members("Mark","Baulo","Encoder")])
#
# session.commit()

members = session.query(Members).all()
for member in members:
    print member
